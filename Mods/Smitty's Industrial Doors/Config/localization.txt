Key,English
door_1, Industrial Door 1
door_2, Industrial Double Door 2
door_3, Industrial Door 3
door_3B, Industrial Door 3B
door_3C, Industrial Door 3C
door_4, Industrial Door 4
door_4B, Industrial Door 4B
door_4C, Industrial Door 4C
door_4D, Industrial Door 4D
door_5, Industrial Door 5
door_6, Industrial Door 6
door_6B, Industrial Door 6B
door_7, Industrial Door 7
door_7B, Industrial Door 7B
door_8, Industrial Door 8
door_8B, Industrial Door 8B
door_8C, Industrial Door 8C
door_9, Industrial Door 9
door_9B, Industrial Door 9B
door_9C, Industrial Door 9C
door_10, Industrial Door 10
door_10B, Industrial Door 10B
door_11, Industrial Door 11
door_11B, Industrial Door 11B
door_11C, Industrial Door 11C
door_12, Industrial Door 12
door_science, Science Door
gate_science, Science Gate
