SMITTY'S INDUSTRIAL DOORS
This is a modlet of Industrial Doors that I animated and ported into 7 Days to Die to offer players more variety of doors for their bases instead of having to use the same tired old models.
There are several double doors and a couple of SCiFI doors that are very durable.

Recipe XMLs are included and all are craftable at the workbench.

LICENSE/USAGE
These models are purchased by me and licensed to me. You are allowed to use this mod on your server and in your mod packs with appropriate credit to me.

CREDIT
Kobra Game Studios for the door models
Guppy/Bdub/Xyth for helping me with issues as this is my first mod

KNOWN ISSUES
None at this time.